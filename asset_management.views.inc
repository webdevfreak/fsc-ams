<?php

/**
 * @file
 * asset_management.views.inc
 */

/**
 * Implements hook_views_data().
 */
function asset_management_views_data() {

  ###
  # AMS Categories
  ###
  // Define the return array.
  $data = array();

  // The outermost keys of $data are Views table names, which should usually
  // be the same as the hook_schema() table names.
  $data['asset_management_category'] = array();

  // The value corresponding to key 'table' gives properties of the table
  // itself.
  $data['asset_management_category']['table'] = array();

  // Within 'table', the value of 'group' (translated string) is used as a
  // prefix in Views UI for this table's fields, filters, etc. When adding
  // a field, filter, etc. you can also filter by the group.
  $data['asset_management_category']['table']['group'] = t('AMS Categories');

  // Within 'table', the value of 'provider' is the module that provides schema
  // or the entity type that causes the table to exist. Setting this ensures
  // that views have the correct dependencies. This is automatically set to the
  // module that implements hook_views_data().
  $data['asset_management_category']['table']['provider'] = 'asset_management';

  // Some tables are "base" tables, meaning that they can be the base tables
  // for views. Non-base tables can only be brought in via relationships in
  // views based on other tables. To define a table to be a base table, add
  // key 'base' to the 'table' array:
  $data['asset_management_category']['table']['base'] = array(
    // Identifier (primary) field in this table for Views.
    'field' => 'id',
    // Label in the UI.
    'title' => t('AMS Categories'),
    // Longer description in the UI. Required.
    'help' => t('AMS Categories table.'),
    'weight' => -10,
  );

  // Plain text field, exposed as a field, sort, filter, and argument.
  $data['asset_management_category']['id'] = array(
    'title' => t('id'),
    'help' => t('Category ID field.'),
    'field' => array(
      // ID of field handler plugin to use.
      'id' => 'standard',
    ),
    'sort' => array(
      // ID of sort handler plugin to use.
      'id' => 'standard',
    ),
    'filter' => array(
      // ID of filter handler plugin to use.
      'id' => 'string',
    ),
    'argument' => array(
      // ID of argument handler plugin to use.
      'id' => 'string',
    ),
  );

  // Plain text field, exposed as a field, sort, filter, and argument.
  $data['asset_management_category']['headline_english'] = array(
    'title' => t('Headline English'),
    'help' => t('Headline English plain text field.'),
    'field' => array(
      // ID of field handler plugin to use.
      'id' => 'standard',
    ),
    'sort' => array(
      // ID of sort handler plugin to use.
      'id' => 'standard',
    ),
    'filter' => array(
      // ID of filter handler plugin to use.
      'id' => 'string',
    ),
    'argument' => array(
      // ID of argument handler plugin to use.
      'id' => 'string',
    ),
  );

  ###
  # AMS Files
  ###
  // Define the return array.
//  $data = array();
  // The outermost keys of $data are Views table names, which should usually
  // be the same as the hook_schema() table names.
  $data['asset_management_files'] = array();

  // The value corresponding to key 'table' gives properties of the table
  // itself.
  $data['asset_management_files']['table'] = array();

  // Within 'table', the value of 'group' (translated string) is used as a
  // prefix in Views UI for this table's fields, filters, etc. When adding
  // a field, filter, etc. you can also filter by the group.
  $data['asset_management_files']['table']['group'] = t('AMS Files');

  // Within 'table', the value of 'provider' is the module that provides schema
  // or the entity type that causes the table to exist. Setting this ensures
  // that views have the correct dependencies. This is automatically set to the
  // module that implements hook_views_data().
  $data['asset_management_files']['table']['provider'] = 'asset_management';

  // Some tables are "base" tables, meaning that they can be the base tables
  // for views. Non-base tables can only be brought in via relationships in
  // views based on other tables. To define a table to be a base table, add
  // key 'base' to the 'table' array:
  $data['asset_management_files']['table']['base'] = array(
    // Identifier (primary) field in this table for Views.
    'field' => 'id',
    // Label in the UI.
    'title' => t('AMS Files'),
    // Longer description in the UI. Required.
    'help' => t('AMS Files table.'),
    'weight' => -10,
  );

  // Plain text field, exposed as a field, sort, filter, and argument.
  $data['asset_management_files']['id'] = array(
    'title' => t('id'),
    'help' => t('File ID field.'),
    'field' => array(
      // ID of field handler plugin to use.
      'id' => 'standard',
    ),
    'sort' => array(
      // ID of sort handler plugin to use.
      'id' => 'standard',
    ),
    'filter' => array(
      // ID of filter handler plugin to use.
      'id' => 'string',
    ),
    'argument' => array(
      // ID of argument handler plugin to use.
      'id' => 'string',
    ),
  );

  // Plain text field, exposed as a field, sort, filter, and argument.
  $data['asset_management_files']['file_name'] = array(
    'title' => t('File Name'),
    'help' => t('File Name plain text field.'),
    'field' => array(
      // ID of field handler plugin to use.
      'id' => 'standard',
    ),
    'sort' => array(
      // ID of sort handler plugin to use.
      'id' => 'standard',
    ),
    'filter' => array(
      // ID of filter handler plugin to use.
      'id' => 'string',
    ),
    'argument' => array(
      // ID of argument handler plugin to use.
      'id' => 'string',
    ),
  );

  return $data;
}
