<?php

/**
 * @file
 * Contains \Drupal\asset_management\Form\FileFormAdd.
 */

namespace Drupal\asset_management\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Xss;

/**
 * AMS Create New File Form.
 */
class FileFormAdd extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'asset_management_file_add';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Query table to get all categories.
    $query_result = db_select('asset_management_category', 'c')
      ->fields('c', array('id', 'headline_english'))
      ->orderBy('id', 'asc')
      ->execute()
      ->fetchAll();

    // Loop through all categories and assign to an array.
    $select_values = array(
      '' => t('--Select category--'),
    );
    foreach ($query_result as $value) {
      $select_values[$value->id] = $value->headline_english;
    }

    // Define form fields.
    $form = array(
      '#attributes' => array('enctype' => 'multipart/form-data'),
    );
    $form['category_id'] = array(
      '#title' => t('Category *'),
      '#type' => 'select',
      '#options' => $select_values,
    );
//    $form['top_heading'] = array(
//      '#markup' => t('<h2>Create new file</h2><br />'),
//    );
    $form['file_details'] = array(
      '#markup' => t('<b>The File</b><br />Information on the file to be '
        . 'uploaded'),
    );
    $form['file_name'] = array(
      '#type' => 'textfield',
      '#size' => 120,
      '#title' => t('File Name *'),
//      '#required' => TRUE,
    );
    $form['description'] = array(
      '#type' => 'textarea',
      '#title' => t('Description [optional]'),
      '#rows' => 5,
//      '#required' => TRUE,
    );
    $form['tags'] = array(
      '#type' => 'textarea',
      '#rows' => 2,
      '#title' => t('Tags (separated with commata) [optional]'),
//      '#required' => TRUE,
    );
    $form['file_upload_details'] = array(
      '#markup' => t('<b>The File</b><br />The file to be uploaded'),
    );
    $validators = array(
//      'file_validate_extensions' => array('docx doc pdf'),
      'file_validate_extensions' => array('pdf zip mp4'),
    );
    $form['ams_file'] = array(
      '#type' => 'managed_file',
      '#name' => 'ams_file',
      '#title' => t('File *'),
      '#size' => 20,
      '#description' => t('The file must not exceed the file size of 512,00 MB'),
      '#upload_validators' => $validators,
      '#upload_location' => 'public://ams_files/',
    );
    $form['limited_access_details'] = array(
      '#markup' => t('<b>Limited Access?</b><br />This content will only be '
        . 'accessible for the selected user groups in the following list. If '
        . 'you select none, the content will be accessible for all visitors of '
        . 'your website (including search engines).'),
    );
    $form['visitors_groups'] = [
      '#type' => 'select',
      '#title' => $this->t('Visitors Groups [optional]'),
      '#options' => [
        'Employees' => $this->t('Employees'),
        'TMCB Users' => $this->t('TMCB Users'),
        'PSC Users' => $this->t('PSC Users'),
        'CB Users' => $this->t('CB Users'),
        'FSC Members' => $this->t('FSC Members'),
        'Playground' => $this->t('Playground'),
      ],
      '#multiple' => TRUE,
      '#size' => 6,
    ];
    $form['content_hidden_heading'] = array(
      '#markup' => t('<b>Hidden?</b><br />Should this content be hidden? [optional]'),
    );
    $form['content_hidden'] = array(
      '#type' => 'checkbox',
      '#title' => t('Yes, hide content'),
    );
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    );
    $form['#cache']['max-age'] = 0;
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('category_id') == NULL) {
      $form_state->setErrorByName('category_id', $this->t('Category.'));
    }
    if ($form_state->getValue('file_name') == NULL) {
      $form_state->setErrorByName('file_name', $this->t('File Name.'));
    }
    if ($form_state->getValue('ams_file') == NULL) {
      $form_state->setErrorByName('ams_file', $this->t('File Upload.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save posted data in the database table.
    $query = db_insert('asset_management_files')
      ->fields(array(
        'category_id' => $form_state->getValue('category_id'),
        'file_name' => Xss::filter($form_state->getValue('file_name')),
        'description' => Xss::filter($form_state->getValue('description')),
        'tags' => Xss::filter($form_state->getValue('tags')),
        'file_managed_id' => $form_state->getValue('ams_file')[0],
        'visitors_groups' => serialize(Xss::filter($form_state->getValue('visitors_groups'))),
        'content_hidden' => $form_state->getValue('content_hidden'),
        'created' => REQUEST_TIME,
      ))
      ->execute();

    // Display success message.
    drupal_set_message('AMS file successfully uploaded.');

    // Redirect.
    $form_state->setRedirect('asset_management.files');
  }

}
