<?php

/**
 * @file
 * Contains \Drupal\asset_management\Form\FileFormDelete.
 */

namespace Drupal\asset_management\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * AMS Delete File Form.
 */
class FileFormDelete extends ConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'asset_management_file_delete';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    // Get all data from the database based on id.
    $result = db_select('asset_management_files', 'f')
      ->fields('f', array('file_name'))
      ->condition('id', $this->id, '=')
      ->execute()
      ->fetchObject();

    //the question to display to the user.
    return t('Do you really want to delete %id?', array('%id' =>
      $result->file_name));
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    //this needs to be a valid route otherwise the cancel link won't appear
    return new Url('asset_management.files');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    //a brief desccription
    return t('Are you sure you want to delete this file?');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Yes');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelText() {
    return $this->t('Cancel');
  }

  /**
   * {@inheritdoc}
   *
   * @param int $id
   *   (optional) The ID of the item to be deleted.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $id = NULL) {
    $this->id = $id;
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Get id from url.
    $edit_id = asset_management_get_id_from_url();

    // Get file id.
    $query_file = db_select('asset_management_files', 'f')
      ->fields('f', array('file_managed_id'))
      ->condition('id', $edit_id)
      ->execute()
      ->fetchObject();

    // Delete file from the hard drive.
    file_delete($query_file->file_managed_id);

    // Delete file from the database table.
    db_delete('asset_management_files')->condition('id', $edit_id)->execute();

    // Display success message.
    drupal_set_message('AMS file successfully deleted.');

    // Redirect.
    $form_state->setRedirect('asset_management.files');
  }

}
