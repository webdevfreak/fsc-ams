<?php

/**
 * @file
 * Contains \Drupal\asset_management\Form\CategoryFormDelete.
 */

namespace Drupal\asset_management\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * AMS Delete Category.
 */
class CategoryFormDelete extends ConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'asset_management_category';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    // Get all data from the database based on id.
    $result = db_select('asset_management_category', 'c')
      ->fields('c', array('headline_english'))
      ->condition('id', $this->id, '=')
      ->execute()
      ->fetchObject();

    //the question to display to the user.
    return t('Do you want to delete %id?', array('%id' =>
      $result->headline_english));
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    //this needs to be a valid route otherwise the cancel link won't appear
    return new Url('asset_management.categories');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    //a brief desccription
    return t('Are you sure you want to delete this category?');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Yes');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelText() {
    return $this->t('Cancel');
  }

  /**
   * {@inheritdoc}
   *
   * @param int $id
   *   (optional) The ID of the item to be deleted.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $id = NULL) {
    $this->id = $id;
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Get id from url.
    $edit_id = asset_management_get_id_from_url();

    // Delete category from the database table.
    db_delete('asset_management_category')->condition('id', $edit_id)->execute();

    // Display success message.
    drupal_set_message('AMS category successfully deleted.');

    // Redirect.
    $form_state->setRedirect('asset_management.categories');
  }

}
