<?php

/**
 * @file
 * Contains \Drupal\asset_management\Form\CategoryFormEdit.
 */

namespace Drupal\asset_management\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Xss;

/**
 * AMS Edit Category.
 */
class CategoryFormEdit extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'asset_management_category';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Get id from url.
    $edit_id = asset_management_get_id_from_url();

    // Get all data from the database based on id.
    $result = db_select('asset_management_category', 'c')
      ->fields('c', array('language_english', 'language_spanish',
        'headline_english', 'headline_spanish', 'content_hidden'))
      ->condition('id', $edit_id, '=')
      ->execute()
      ->fetchObject();

    // Define form fields.
    $form['languages'] = array(
      '#markup' => t('<b>Languages</b> *<br /> In what language was this content'
        . ' created?'),
      '#required' => TRUE,
    );
    $form['language_english'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('English'),
      '#default_value' => $result->language_english,
    );
    $form['language_spanish'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Spanish'),
      '#default_value' => $result->language_spanish,
    );
    $form['ams_category'] = array(
      '#markup' => t('<b>The AMS Category</b><br />Information and content of'
        . ' the AMS Category to be created'),
    );
    $form['headline_english'] = array(
      '#type' => 'textfield',
      '#size' => 80,
      '#title' => t('Headline - English *'),
      '#default_value' => $result->headline_english,
//      '#required' => TRUE,
    );
    $form['headline_spanish'] = array(
      '#type' => 'textfield',
      '#size' => 80,
      '#title' => t('Headline - Spanish'),
      '#default_value' => $result->headline_spanish,
//      '#required' => TRUE,
    );
    $form['content_hidden_heading'] = array(
      '#markup' => t('<b>Hidden?</b><br />Should this content be hidden? [optional]'),
    );
    $form['content_hidden'] = array(
      '#type' => 'checkbox',
      '#title' => t('Yes, hide content'),
      '#default_value' => $result->content_hidden,
    );
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    );
    $form['#cache']['max-age'] = 0;
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('language_english') == NULL) {
      $form_state->setErrorByName('language_english', $this->t('Select language.'));
    }
    if ($form_state->getValue('headline_english') == NULL) {
      $form_state->setErrorByName('headline_english', $this->t('Headline - English.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Get id from url.
    $edit_id = asset_management_get_id_from_url();

    // Save posted data in the database table.
    $query = db_merge('asset_management_category')
      ->key(array('id' => $edit_id))
      ->fields(array(
        'language_english' => $form_state->getValue('language_english'),
        'language_spanish' => $form_state->getValue('language_spanish'),
        'headline_english' => Xss::filter($form_state->getValue(
            'headline_english')),
        'headline_spanish' => Xss::filter($form_state->getValue(
            'headline_spanish')),
        'content_hidden' => $form_state->getValue('content_hidden'),
      ))
      ->execute();

    // Display success message.
    drupal_set_message('AMS category successfully updated.');

    // Redirect.
    $form_state->setRedirect('asset_management.categories');
  }

}
