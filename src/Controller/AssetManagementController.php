<?php

/**
 * @file
 * Contains \Drupal\page_example\Controller\AssetManagementController.
 */

namespace Drupal\asset_management\Controller;

use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Controller routines for page example routes.
 */
class AssetManagementController {

  /**
   *
   * Constructs a simple page.
   * The router _controller callback, maps the path 'examples/asset_management/simple'
   * to this method.
   * _controller callbacks return a renderable array for the content area of the
   * page. The theme system will later render and surround the content with the
   * appropriate blocks, navigation, and styling.
   */
  public function simple() {
    return [
      '#markup' => t('Simple page: The quick brown fox jumps over the lazy dog.'),
    ];
  }

}
